package com.sigital.springjwt.service;

import com.sigital.springjwt.models.User;

public interface UserService {
    /**
     * Permet de créer un utilisateur
     * @param user l'utilisateur
     * @return l'utilisateur crée
     */
    User createUser(User user);
    User findByUsername(String username);
    Boolean  existsByEmail(String email);
    Boolean existsByUsername(String username);



}
