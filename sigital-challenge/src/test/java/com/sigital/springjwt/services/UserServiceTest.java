package com.sigital.springjwt.services;

import com.sigital.springjwt.exception.UserNotFoundException;
import com.sigital.springjwt.models.User;
import com.sigital.springjwt.repository.UserRepository;
import com.sigital.springjwt.service.UsersServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    protected UserRepository userRepository ;

    @InjectMocks
    public UsersServiceImpl userService;

    @Test
    public void shouldCreateUser() {

        //Given
        User user = new User();
        user.setId(1L);
        user.setUsername("zakaria");
        user.setEmail("zakaria@gmail.com");

        //When
        when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
        User userSaved = this.userService.createUser(user);

        //Then
        verify(userRepository,times(1)).save(Mockito.any(User.class));
        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(userCaptor.capture());

        assertEquals(userSaved, user);
    }


    @Test
    public void shouldGetExistingUserByUsername() {

        //Given
        User zakaria = new User();
        zakaria.setUsername("2akaria");
        zakaria.setEmail("zakaria@gmail.com");
        zakaria.setPassword("user123");

        //When
        when(userRepository.findByUsername("zakaria")).thenReturn(Optional.of(zakaria));

        User result = this.userService.findByUsername("zakaria");

        //Then
        assertEquals(result, zakaria);
        verify(userRepository,times(1)).findByUsername(("zakaria"));

    }

    @Test(expected = UserNotFoundException.class)
    public void shouldNotGetInexistingUserByUsername() {

        when(userRepository.findByUsername("Batman")).thenReturn(Optional.empty());

        this.userService.findByUsername("Batman");

    }

}
