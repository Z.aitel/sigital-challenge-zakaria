import { Component, OnInit } from '@angular/core';

import { TokenStorageService } from '../services/token-storage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './listUsers.component.html',
  styleUrls: ['./listUsers.component.css']
})
export class ListUsersComponent implements OnInit {
  currentUser: any;

  constructor(private token: TokenStorageService) { }

  ngOnInit() {
    this.currentUser = this.token.getUser();
  }
}
